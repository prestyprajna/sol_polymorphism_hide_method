﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Polymorphism_Hide_Method
{
    class Program
    {
        static void Main(string[] args)
        {
            //Presty prestyObj = new Presty();  //tight -couple
            //prestyObj.TestMethod();

            //loose -couple
            //Presty prestyObj1 = new Mangal();
            //prestyObj1.TestMethod();

            Mangal mangalObj = new Ravina();
            mangalObj.TestMethod();
        }
    }

    public class Presty
    {
        public virtual void TestMethod()
        {
            Console.WriteLine("presty method");
        }
    }

    public class Mangal : Presty
    {
        public new virtual  void TestMethod()
        {
            //base.TestMethod();
            Console.WriteLine("mangal method");
        }
    }

    public class Ravina : Mangal
    {
        public new virtual void TestMethod()
        {
            //base.TestMethod();
            Console.WriteLine("ravina method");
        }
    }
}
